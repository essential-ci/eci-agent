import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.springframework.boot.gradle.tasks.bundling.BootJar
import pl.allegro.tech.build.axion.release.domain.VersionConfig

plugins {
    kotlin("jvm") version "1.4.10"
    kotlin("kapt") version "1.4.10"
    kotlin("plugin.spring") version "1.4.10"
    id("idea")
    id("org.springframework.boot") version "2.3.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("pl.allegro.tech.build.axion-release") version "1.12.0"
}

group = "pl.jdudycz.eci"
version = scmVersion.version
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/groups/essential-ci/-/packages/maven")
        name = "essential-ci"
        credentials(HttpHeaderCredentials::class) {
            name = "Deploy-Token"
            value = System.getenv("GITLAB_DEPLOY_TOKEN")
        }
        authentication {
            create<HttpHeaderAuthentication>("header")
        }
    }
}

val eciCommonVersion = "0.9.3"
val dockerClientVersion = "3.2.5"
val jgitVersion = "5.9.0.202009080501-r"
val uuidVersion = "4.0.1"
val apacheCommonsVersion = "3.11"

dependencies {
    implementation("pl.jdudycz.eci:eci-common:${eciCommonVersion}")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.kafka:spring-kafka")
    implementation("io.projectreactor.kafka:reactor-kafka")
    implementation("io.projectreactor.addons:reactor-extra")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.github.docker-java:docker-java:${dockerClientVersion}")
    implementation("com.github.docker-java:docker-java-transport-httpclient5:${dockerClientVersion}")
    implementation("org.eclipse.jgit:org.eclipse.jgit:${jgitVersion}")
    implementation("com.fasterxml.uuid:java-uuid-generator:${uuidVersion}")
    implementation("org.apache.commons:commons-lang3:${apacheCommonsVersion}")
    kapt("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
}

tasks{
    test{
        useJUnitPlatform()
    }
    compileKotlin{
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }
    bootBuildImage{
        imageName = "${System.getenv("ECI_DOCKER_REGISTRY")}/${project.name}:${project.version}"
    }

    bootJar{
        layered()
    }
}

configure<VersionConfig> {
    versionIncrementer("incrementMinor")
}
