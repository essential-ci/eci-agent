set -e

./gradlew clean build
IMAGE_NAME=jdudycz/essential-ci-agent:$(./gradlew cV -q -Prelease.quiet)
docker build . -t ${IMAGE_NAME}
docker login --username=${DOCKER_HUB_USER}
docker push ${IMAGE_NAME}
