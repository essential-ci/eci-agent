package pl.jdudycz.eci.agent.registration.model

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("pl.jdudycz.eci.agent.registration")
data class RegistrationProps(val name: String, val registrationCode: String)
