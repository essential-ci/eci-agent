package pl.jdudycz.eci.agent.registration

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters.fromValue
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.toEntity
import pl.jdudycz.eci.agent.registration.model.RegistrationRequest
import reactor.core.publisher.Mono

@Service
class RegistrationClient(private val httpClient: WebClient) {

    fun register(request: RegistrationRequest): Mono<String> =
            httpClient.post()
                    .uri(REGISTRATION_ENDPOINT)
                    .body(fromValue(request))
                    .exchange()
                    .flatMap(::handleResponse)

    private fun handleResponse(response: ClientResponse): Mono<String> =
            response.toEntity<String>().map { if (it.statusCode.is2xxSuccessful) it.body else throw Exception(it.body) }


    companion object {
        private const val REGISTRATION_ENDPOINT = "/api/agent/register"
    }
}
