package pl.jdudycz.eci.agent.registration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

@Configuration
class WebClientConfig {

    @Bean
    fun webClient(httpClient: HttpClient,
                  @Value("\${pl.jdudycz.eci.agent.remote-url}")
                  url: String): WebClient =
            WebClient.builder().clientConnector(ReactorClientHttpConnector(httpClient)).baseUrl(url).build()

}
