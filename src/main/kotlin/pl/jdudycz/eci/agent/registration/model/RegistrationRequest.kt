package pl.jdudycz.eci.agent.registration.model

data class RegistrationRequest(val name: String,
                               val registrationCode: String,
                               val publicIp: String,
                               val apiKey: String)
