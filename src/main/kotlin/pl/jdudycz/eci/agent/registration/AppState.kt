package pl.jdudycz.eci.agent.registration

interface AppState {
    val agentId: String
    val apiKey: String
}
