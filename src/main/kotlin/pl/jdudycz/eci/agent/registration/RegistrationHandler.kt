package pl.jdudycz.eci.agent.registration

import org.apache.commons.lang3.RandomStringUtils
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import pl.jdudycz.eci.agent.registration.model.RegistrationProps
import pl.jdudycz.eci.agent.registration.model.RegistrationRequest
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct
import kotlin.system.exitProcess

@Service
class RegistrationHandler(private val registrationProps: RegistrationProps,
                          private val registrationClient: RegistrationClient,
                          private val appState: ConfigurableAppState) {

    @PostConstruct
    fun register() {
        getPublicIp()
                .map(::constructRequest)
                .flatMap(registrationClient::register)
                .doOnError {
                    log.error("Failed to register agent: ${it.localizedMessage}")
                    exitProcess(1)
                }
                .doOnSuccess {
                    log.info("Agent has been registered successfully. Assigned id: $it")
                    appState.agentId = it
                }
                .block()
    }

    private fun constructRequest(publicIp: String): RegistrationRequest {
        val key = generateApiKey()
        appState.apiKey = key
        log.debug("Using api key: ${appState.apiKey}")
        return RegistrationRequest(
                registrationProps.name,
                registrationProps.registrationCode,
                publicIp,
                key)
    }

    private fun getPublicIp(): Mono<String> {
        val client = WebClient.create()
        return Flux.fromIterable(publicIpProviders)
                .flatMap { client.callForPublicIp(it) }.next()
                .switchIfEmpty(Mono.error(Exception("Couldn't get hosts public ip")))
    }

    private fun WebClient.callForPublicIp(url: String) = get()
            .uri(url)
            .exchange()
            .flatMap {
                if (!it.statusCode().is2xxSuccessful) Mono.empty()
                else it.bodyToMono<String>()
            }

    private fun generateApiKey(): String = RandomStringUtils.randomAlphanumeric(API_KEY_LENGTH)

    companion object {
        private const val API_KEY_LENGTH = 16
        private val publicIpProviders = listOf("https://ifconfig.me", "https://ipecho.net/plain", "https://api.ipify.org")
        private val log = logger<RegistrationHandler>()
    }
}
