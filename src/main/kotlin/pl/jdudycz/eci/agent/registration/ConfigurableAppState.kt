package pl.jdudycz.eci.agent.registration

import org.springframework.stereotype.Service

@Service
class ConfigurableAppState : AppState {
    override lateinit var agentId: String
    override lateinit var apiKey: String
}
