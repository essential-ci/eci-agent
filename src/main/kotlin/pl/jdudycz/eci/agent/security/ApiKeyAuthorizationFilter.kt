package pl.jdudycz.eci.agent.security

import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import pl.jdudycz.eci.agent.registration.AppState
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Mono

@Component
class ApiKeyAuthorizationFilter(private val state: AppState) : WebFilter {

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val apiKey = exchange.request.headers[API_KEY_HEADER]?.first()
        return if (apiKey != state.apiKey) {
            exchange.response.statusCode = HttpStatus.UNAUTHORIZED
            Mono.fromCallable { dataBufferFactory.wrap(UNAUTHORIZED_MESSAGE) }
                    .transform(exchange.response::writeWith)
        } else chain.filter(exchange)
    }

    companion object {
        private const val API_KEY_HEADER = "Agent-Api-Key"
        private val dataBufferFactory = DefaultDataBufferFactory()
        private val UNAUTHORIZED_MESSAGE = "Invalid api key in Agent-Api-Key header".toByteArray()
    }
}
