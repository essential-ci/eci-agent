package pl.jdudycz.eci.agent.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain


@Configuration
class SecurityConfig {

    @Bean
    fun securityFilterChain(httpSecurity: ServerHttpSecurity, authFilter: ApiKeyAuthorizationFilter): SecurityWebFilterChain =
            httpSecurity.authorizeExchange()
                    .pathMatchers("/**").permitAll()
                    .and()
                    .addFilterAt(authFilter, SecurityWebFiltersOrder.AUTHORIZATION)
                    .httpBasic().disable()
                    .csrf().disable()
                    .cors().disable()
                    .formLogin().disable()
                    .logout().disable()
                    .build()
}
