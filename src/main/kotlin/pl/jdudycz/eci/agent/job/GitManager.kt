package pl.jdudycz.eci.agent.job


import org.eclipse.jgit.api.Git
import org.eclipse.jgit.storage.file.FileBasedConfig
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.eclipse.jgit.util.FS
import org.springframework.stereotype.Service
import pl.jdudycz.eci.agent.job.model.CheckoutData
import reactor.core.publisher.Mono
import java.io.File


@Service
class GitManager {

    fun checkoutRepo(jobId: String, checkoutData: CheckoutData): Mono<File> =
            checkout(checkoutData, File("$CHECKOUT_DIR/$jobId"))

    private fun checkout(checkoutData: CheckoutData, localDir: File): Mono<File> = Mono.fromCallable {
        val gitRepo = Git.init().setDirectory(localDir).call()
        val fetchRequest = gitRepo.fetch()
                .setRemote(checkoutData.repoUrl)
                .setRefSpecs(checkoutData.ref)
        checkoutData.token?.also {
            fetchRequest.setCredentialsProvider(
                    UsernamePasswordCredentialsProvider(USERNAME_PLACEHOLDER, it)
            )
        }
        fetchRequest.call()
        gitRepo.checkout().setName(checkoutData.checkoutSha).call().let { localDir }
    }

    companion object {
        private const val CHECKOUT_DIR = "/tmp/eci-agent/clone"
        private const val USERNAME_PLACEHOLDER = "placeholder"
    }
}
