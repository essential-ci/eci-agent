package pl.jdudycz.eci.agent.job

import com.github.dockerjava.api.DockerClient
import org.springframework.stereotype.Service
import pl.jdudycz.eci.agent.job.model.Job
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.LinkedBlockingQueue

@Service
class JobStore(private val dockerClient: DockerClient) {

    private val queue = LinkedBlockingQueue<Job>()
    private val runningJobs = ConcurrentHashMap<String, String>()

    fun enqueue(job: Job) = queue.add(job)

    fun dequeue(): Job = queue.take().also {
        runningJobs[it.id] = ""
    }

    fun assignContainer(jobId: String, containerId: String) {
        runningJobs[jobId] = containerId
    }

    fun cancelJob(jobId: String) {
        if (queue.removeIf { it.id == jobId }) return
        runningJobs[jobId]?.let {
            if (it.isNotEmpty()) dockerClient.removeContainerCmd(it).withForce(true).exec()
        }
        completeJob(jobId)
    }

    fun completeJob(jobId: String) {
        runningJobs.remove(jobId)
    }

    fun getRunningCount(): Int = runningJobs.size

    fun getQueuedCount(): Int = queue.size
}
