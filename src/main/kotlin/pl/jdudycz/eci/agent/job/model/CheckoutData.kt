package pl.jdudycz.eci.agent.job.model


data class CheckoutData(val ref: String,
                        val checkoutSha: String,
                        val repoUrl: String,
                        val token: String?)
