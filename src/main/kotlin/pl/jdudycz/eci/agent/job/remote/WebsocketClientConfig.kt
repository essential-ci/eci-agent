package pl.jdudycz.eci.agent.job.remote

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient
import org.springframework.web.reactive.socket.client.WebSocketClient
import reactor.netty.http.client.HttpClient

@Configuration
class WebsocketClientConfig {

    @Bean
    fun websocketClient(httpClient: HttpClient): WebSocketClient = httpClient.let(::ReactorNettyWebSocketClient)
}
