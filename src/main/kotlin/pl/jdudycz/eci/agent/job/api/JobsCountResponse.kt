package pl.jdudycz.eci.agent.job.api

data class JobsCountResponse(val queuedCount: Int, val runningCount: Int)
