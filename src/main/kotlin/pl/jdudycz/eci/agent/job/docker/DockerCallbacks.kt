package pl.jdudycz.eci.agent.job.docker

import com.github.dockerjava.api.async.ResultCallback
import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.api.model.PullResponseItem
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.taskFailed
import pl.jdudycz.eci.common.domain.event.taskOutput
import pl.jdudycz.eci.common.domain.event.taskSucceeded
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.FluxSink
import reactor.core.publisher.MonoSink
import java.util.concurrent.atomic.AtomicReference


class ContainerLogCallback(private val taskId: String,
                           private val eventSink: FluxSink<Event>,
                           private val completionSink: MonoSink<Unit>) : ResultCallback.Adapter<Frame>() {

    private val lastOutputFailed = AtomicReference(false)

    override fun onNext(frame: Frame) {
        val content = String(frame.payload).trim()
        eventSink.next(taskOutput(taskId, content))
        // cannot determine which frame is the last one
        lastOutputFailed.set(content == "# command failed")
    }

    override fun onComplete() {
        super.onComplete()
        if (!lastOutputFailed.get()) {
            log.info("Task $taskId succeeded")
            eventSink.next(taskSucceeded(taskId))
            completionSink.success()
        } else {
            log.info("Task $taskId failed")
            eventSink.next(taskFailed(taskId))
            completionSink.error(Exception("Task $taskId failed"))
        }
    }

    companion object {
        private val log = logger<ContainerLogCallback>()
    }
}

class ImagePullCallback(private val imageName: String) : ResultCallback.Adapter<PullResponseItem>() {

    override fun onComplete() {
        log.debug("Image $imageName pulled")
        super.onComplete()
    }

    companion object {
        private val log = logger<ImagePullCallback>()
    }
}
