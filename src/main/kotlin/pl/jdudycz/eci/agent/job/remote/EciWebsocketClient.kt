package pl.jdudycz.eci.agent.job.remote

import org.springframework.stereotype.Service
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import org.springframework.web.reactive.socket.client.WebSocketClient
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.net.URI
import java.time.Duration

@Service
class EciWebsocketClient(private val websocketClient: WebSocketClient) {

    fun sendEvents(events: Flux<Event>): Mono<Void> =
            websocketClient.execute(URI.create(AGENT_WS_ENDPOINT)) { it.sendEvents(events) }

    private fun WebSocketSession.sendEvents(events: Flux<Event>) =
            // TODO required to keep connection open for long jobs. has to be reworked
            Flux.create<WebSocketMessage> { sink ->
                val outputCompleted = events
                        .map { binaryMessage(it) }
                        .doOnError(sink::error)
                        .doOnComplete(sink::complete)
                        .doOnNext(sink::next)
                        .then()

                Flux.interval(Duration.ofSeconds(10))
                        .takeUntilOther(outputCompleted)
                        .subscribe { sink.next(pingMessage { it.allocateBuffer() }) }
            }
                    .transform { send(it) }
                    .then()

    private fun WebSocketSession.binaryMessage(event: Event): WebSocketMessage =
            binaryMessage { it.wrap(event.toByteArray()) }

    companion object {
        private val log = logger<EciWebsocketClient>()
        private const val AGENT_WS_ENDPOINT = "/api/ws/agent"
    }
}
