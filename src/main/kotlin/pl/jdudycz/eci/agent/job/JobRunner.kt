package pl.jdudycz.eci.agent.job

import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactor.mono
import org.springframework.stereotype.Service
import pl.jdudycz.eci.agent.job.model.Job
import pl.jdudycz.eci.agent.job.model.Task
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.jobFailed
import pl.jdudycz.eci.common.domain.event.jobStarted
import pl.jdudycz.eci.common.domain.event.jobSucceeded
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono
import java.io.File

@Service
class JobRunner(private val taskRunner: TaskRunner,
                private val repoManager: GitManager) {

    fun run(job: Job): Flux<Event> = Flux.create { eventSink ->
        log.info("Executing job ${job.id} for ${job.checkoutData.repoUrl}/${job.checkoutData.ref}")
        eventSink.next(jobStarted(job.id))
        repoManager
                .checkoutRepo(job.id, job.checkoutData)
                .flatMap { runTasks(job, it, eventSink) }
                .subscribe()
    }

    private fun runTasks(job: Job, repoDir: File, eventSink: FluxSink<Event>) =
            runTasksSequentially(job, repoDir, eventSink)
                    .doOnSuccess {
                        eventSink.next(jobSucceeded(job.id))
                        cleanup(repoDir, eventSink)
                        log.info("Job ${job.id} succeeded")
                    }
                    .resumeOnError {
                        eventSink.next(jobFailed(job.id, it.localizedMessage))
                        cleanup(repoDir, eventSink)
                        log.info("Job ${job.id} failed: ${it.localizedMessage}")
                    }

    private fun runTasksSequentially(job: Job, localRepo: File, eventSink: FluxSink<Event>): Mono<Unit> =
            mono {
                job.tasks.sortedBy(Task::ordinal).forEach {
                    taskRunner.runWith(eventSink)(job.id, it, localRepo).awaitFirstOrNull()
                }
            }

    private fun cleanup(repoDir: File, eventSink: FluxSink<Event>) {
        repoDir.deleteRecursively()
        eventSink.complete()
    }

    companion object {
        private val log = logger<JobRunner>()
    }
}
