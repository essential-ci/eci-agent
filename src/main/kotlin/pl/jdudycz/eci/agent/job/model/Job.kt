package pl.jdudycz.eci.agent.job.model

data class Job(
        val id: String,
        val tasks: List<Task>,
        val checkoutData: CheckoutData
)
