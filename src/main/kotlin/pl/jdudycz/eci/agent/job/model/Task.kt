package pl.jdudycz.eci.agent.job.model

data class Task(val id: String,
                val ordinal: Int,
                val name: String,
                val image: String,
                val variables: Map<String, Any>,
                val secretVariables: Map<String, Any>,
                val commands: List<String>)
