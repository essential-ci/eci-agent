package pl.jdudycz.eci.agent.job

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.model.Bind
import com.github.dockerjava.api.model.HostConfig
import com.github.dockerjava.api.model.Volume
import org.springframework.stereotype.Service
import pl.jdudycz.eci.agent.job.docker.ContainerLogCallback
import pl.jdudycz.eci.agent.job.docker.ImagePullCallback
import pl.jdudycz.eci.agent.job.model.Task
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.taskStarted
import pl.jdudycz.eci.common.util.asEnvString
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono
import java.io.File

@Service
class TaskRunner(private val dockerClient: DockerClient,
                 private val jobStore: JobStore) {

    fun runWith(eventSink: FluxSink<Event>): (String, Task, File) -> Mono<Void> = { jobId, task, repoDir ->
        pullImage(task.image)
                .flatMap { createContainer(jobId, task, repoDir) }
                .flatMap { run(it, task, eventSink) }
                .then()
    }

    private fun pullImage(image: String) = Mono.fromCallable {
        log.debug("Pulling image $image")
        dockerClient
                .pullImageCmd(image)
                .exec(ImagePullCallback(image))
                .awaitCompletion()
    }

    private fun createContainer(jobId: String, task: Task, repoLocalDir: File): Mono<String> =
            Mono.fromCallable {
                val repoVolume = Volume(WORKING_DIR)
                val dockerSocketVolume = Volume(DOCKER_SOCKET_PATH)
                val hostConfig = HostConfig.newHostConfig()
                        .withAutoRemove(true)
                        .withBinds(
                                Bind(repoLocalDir.absolutePath, repoVolume),
                                Bind(DOCKER_SOCKET_PATH, dockerSocketVolume),
                        )
                dockerClient
                        .createContainerCmd(task.image)
                        .withHostConfig(hostConfig)
                        .withEnv((task.secretVariables).asEnvString())
                        .withName("task-${task.id}")
                        .withWorkingDir(WORKING_DIR)
                        .withEntrypoint("/bin/sh", "-c", parseScript(task))
                        .withVolumes(repoVolume, dockerSocketVolume)
                        .exec().id
                        .also { jobStore.assignContainer(jobId, it) }
            }

    private fun parseScript(task: Task) =
            task.variables
                    .map { "export ${it.key}=${it.value}" }
                    .plus(decorateWithDescriptors(task.commands, task.secretVariables.keys))
                    .joinToString("\n")

    private fun decorateWithDescriptors(commands: List<String>,
                                        secretVariablesNames: Set<String>): List<String> = commands.map {
        """
            ${generateDescriptor(it, secretVariablesNames)}
            $it
            if [ ${'$'}? -eq 0 ]; then
                echo \# command succeeded
            else
                echo \# command failed
                exit 1
            fi
            """.trimIndent()
    }

    private fun generateDescriptor(command: String, secretVariablesNames: Set<String>) =
            command.escapeVariables(secretVariablesNames).let { "echo \\# $it" }


    fun String.escapeVariables(secretVariablesNames: Set<String>): String =
            secretVariablesNames.fold(this) { com, v -> com.escapeVariable(v) }

    private fun String.escapeVariable(variable: String) =
            replace(Regex("\\\$\\{?$variable\\b}?"), "\\\\\\$\\{$variable}")

    private fun run(containerId: String, task: Task, eventSink: FluxSink<Event>): Mono<Void> =
            startContainer(containerId, task.id, eventSink)
                    .then(attachToLogs(containerId, task.id, eventSink))
                    .then()

    private fun startContainer(containerId: String, taskId: String, eventSink: FluxSink<Event>): Mono<Unit> =
            Mono.fromCallable {
                dockerClient.startContainerCmd(containerId).exec()
                eventSink.next(taskStarted(taskId))
                log.info("Task $taskId started")
            }

    private fun attachToLogs(containerId: String, taskId: String, eventSink: FluxSink<Event>): Mono<Unit> =
            Mono.create {
                dockerClient.logContainerCmd(containerId)
                        .withStdErr(true)
                        .withStdOut(true)
                        .withFollowStream(true)
                        .exec(ContainerLogCallback(taskId, eventSink, it))
            }

    companion object {
        private val log = logger<TaskRunner>()
        private const val WORKING_DIR = "/repo"
        private const val DOCKER_SOCKET_PATH = "/var/run/docker.sock"
    }
}
