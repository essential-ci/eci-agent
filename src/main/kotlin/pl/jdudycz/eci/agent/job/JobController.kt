package pl.jdudycz.eci.agent.job

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import pl.jdudycz.eci.agent.job.model.Job
import pl.jdudycz.eci.agent.job.remote.EciWebsocketClient
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import reactor.core.Disposable
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toMono
import java.time.Duration
import javax.annotation.PostConstruct

// TODO this whole thing has to be rewritten, no time for thinking now :/
@Service
class JobController(@Value("\${pl.jdudycz.eci.agent.max-running-jobs}")
                    private val maxRunningJobs: Int,
                    private val websocketClient: EciWebsocketClient,
                    private val store: JobStore,
                    private val jobRunner: JobRunner) {

    @PostConstruct
    fun init() {
        // TODO maybe parallel flux instead?
        for (i in 1..maxRunningJobs) startControllerInstance()
    }

    private fun startControllerInstance(): Disposable =
            Flux.interval(Duration.ofSeconds(1))
                    .filter { store.getQueuedCount() > 0 }
                    .map { store.dequeue() }
                    .flatMap(::run)
                    .resumeOnError { log.warn("Error during controller execution", it) }
                    .subscribe()

    private fun run(job: Job) =
            websocketClient.sendEvents(jobRunner.run(job)).then(store.completeJob(job.id).toMono())

    companion object {
        private val log = logger<JobController>()
    }
}
