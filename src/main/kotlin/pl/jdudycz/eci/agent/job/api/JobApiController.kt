package pl.jdudycz.eci.agent.job.api

import org.springframework.context.annotation.DependsOn
import org.springframework.web.bind.annotation.*
import pl.jdudycz.eci.agent.job.JobStore
import pl.jdudycz.eci.agent.job.model.Job
import pl.jdudycz.eci.agent.job.remote.EciWebsocketClient
import pl.jdudycz.eci.agent.registration.AppState
import pl.jdudycz.eci.common.domain.event.jobAccepted
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@RestController
@RequestMapping("/api/job")
@DependsOn("registrationHandler")
class JobApiController(private val jobStore: JobStore,
                       private val websocketClient: EciWebsocketClient,
                       private val idProvider: AppState) {

    @PostMapping
    fun runJob(@RequestBody body: Job): Mono<Void> =
            websocketClient
                    .sendEvents(Flux.just(jobAccepted(body.id, idProvider.agentId)))
                    .doOnSuccess { jobStore.enqueue(body) }

    @PostMapping("{jobId}/cancel")
    fun cancelJob(@PathVariable jobId: String): Mono<Unit> =
            Mono.fromCallable { jobStore.cancelJob(jobId) }

    @GetMapping("count")
    fun getJobsCount(): Mono<JobsCountResponse> = Mono.fromCallable {
        JobsCountResponse(jobStore.getQueuedCount(), jobStore.getRunningCount())
    }
}
