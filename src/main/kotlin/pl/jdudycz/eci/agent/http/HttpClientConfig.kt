package pl.jdudycz.eci.agent.http

import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.InsecureTrustManagerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.util.logger
import reactor.netty.http.client.HttpClient


@Configuration
class HttpClientConfig {

    @Bean
    fun httpClient(@Value("\${pl.jdudycz.eci.agent.remote-url}") url: String): HttpClient {
        val sslContext = SslContextBuilder
                .forClient()
                // TODO load eci root ca instead
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build()
        return HttpClient
                .create()
                .doOnResponseError { _, e -> log.warn("Request error", e) }
                .doOnRequestError { _, e -> log.warn("Request error", e) }
                .secure { t -> t.sslContext(sslContext) }
                .baseUrl(url)
    }

    companion object {
        private val log = logger<HttpClientConfig>()
    }
}
