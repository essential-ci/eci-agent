FROM openjdk:11-jdk-slim as builder
WORKDIR /app
COPY build/libs/eci-agent-*.jar eci-agent.jar
RUN java -Djarmode=layertools -jar eci-agent.jar extract

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=builder app/dependencies/ ./
COPY --from=builder app/snapshot-dependencies/ ./
COPY --from=builder app/spring-boot-loader/ ./
COPY --from=builder app/application/ ./

ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
